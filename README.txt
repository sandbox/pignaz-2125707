Drupal deny_cotnent_view module:
------------------------
Maintainers:
  Nicola Pignatelli (http://drupal.org/user/471908)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
Deny Content View allows to deny the display of a content type published 
on node page.


Features:
---------

The Deny Content View module:

* Choose which content type to deny the view of content type on node page.


Installation:
------------
1. Download and unpack the Deny Content View module directory in your modules 
   folder (this will usually be "sites/all/modules/").
2. Go to "Administer" -> "Modules" and enable the Deny Content View module.


Configuration:
-------------
Go to Administration -> People -> Permissions 
(http://your-domain.com/admin/people/permissions) to configure permission 
for every content type.
