<?php

/**
 * @file
 * A deny content view module that allows to deny the display of a 
 * content type published.
 */

/**
 * Implements hook_help().
 */
function deny_content_view_help($path, $arg) {
  switch ($path) {
    case "admin/help#deny_content_view":

      return '<p>' . t("Deny the display of a content type published.") . '</p>';
  }
}

/**
 * Implements hook_modules_enabled().
 */
function deny_content_view_modules_enabled($modules) {
  foreach ($modules as $module) {
    // Delete role permission about deny_content_view created by core.
    db_delete('role_permission')
     ->condition('module', $module)
     ->execute();
  }
}

/**
 * Implements hook_permission().
 */
function deny_content_view_permission() {

  $permissions = array();

  $types = node_type_get_types();
  foreach ($types as $type => $content_type) {
    $permissions['deny ' . $type . ' content view'] = array(
      'title' => t('!content_type: Deny view published content.', array('!content_type' => $content_type->name)),
      'description' => t('This permission allows you to deny the display of content type !content_type', array('!content_type' => $content_type->name)),
    );
  }

  return $permissions;
}

/**
 * Implements hook_node_access().
 */
function deny_content_view_node_access($node, $op, $account) {

  if ($op == 'view' && user_access('deny ' . $node->type . ' content view', $account)) {

    return NODE_ACCESS_DENY;
  }

  return NODE_ACCESS_IGNORE;
}
